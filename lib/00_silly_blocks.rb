# def reverser
#   yield.split.map!(&:reverse).join(" ")
# end
#
# def adder(surplus = 1)
#   yield + surplus
# end
#
# def repeater(times = 1)
#   times.times do
#     yield
#   end
# end




# def reverser
#   words = yield.split
#   words.map(&:reverse).join(" ")
# end



def reverser(&prc)
  words = prc.call.split
  words.map(&:reverse).join(" ")
end

def adder(adder = 1, &prc)
  prc.call + adder
end

def repeater(times = 1, &prc)
  times.times {prc.call}
end
