# def measure(times = 1, &prc)
#   start_time = Time.now
#   times.times {prc.call}
#   (Time.now - start_time) / times
# end
#
#

# def measure(times = 1)
#   start_time = Time.now
#   times.times { yield }
#   (Time.now - start_time) / times
# end


def measure(times = 1, &prc)
  start_time = Time.now
  times.times {prc.call}
  elapsed_time = (Time.now - start_time) / times
end
